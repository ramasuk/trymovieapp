package id.co.trymovieapp.model

data class Dates(
    val maximum: String,
    val minimum: String
)