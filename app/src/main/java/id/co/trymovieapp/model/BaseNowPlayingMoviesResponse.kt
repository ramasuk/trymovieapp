package id.co.trymovieapp.model

data class BaseNowPlayingMoviesResponse<T>(
    val dates: Dates,
    val page: Int,
    val data: List<NowPlayingMoviesResponse>,
    val total_pages: Int,
    val total_results: Int
)