package id.co.trymovieapp.model

import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MoviesRepository {
    fun getAllMovies(): MutableLiveData<ArrayList<NowPlayingMoviesResponse>> {
        val moviesData = MutableLiveData<ArrayList<NowPlayingMoviesResponse>>()
        val listMovies = ArrayList<NowPlayingMoviesResponse>()

        NetworkRepository.movieApi().fetchMovies()
            .enqueue(object : Callback<BaseNowPlayingMoviesResponse<NowPlayingMoviesResponse>> {
                override fun onFailure(
                    call: Call<BaseNowPlayingMoviesResponse<NowPlayingMoviesResponse>>,
                    t: Throwable
                ) {
                    moviesData.postValue(null)
                }

                override fun onResponse(
                    call: Call<BaseNowPlayingMoviesResponse<NowPlayingMoviesResponse>>,
                    response: Response<BaseNowPlayingMoviesResponse<NowPlayingMoviesResponse>>
                ) {
                    if (response.isSuccessful) {
                        val listMovieSize = response.body()?.data?.size as Int
                        for (i in 0 until listMovieSize) {
                            listMovies.add(response.body()?.data?.get(i) as NowPlayingMoviesResponse)
                        }
                        moviesData.postValue(listMovies)
                    }
                }
            })

        return moviesData

    }
}